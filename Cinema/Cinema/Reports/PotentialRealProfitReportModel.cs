﻿using System.Collections.Generic;

namespace Cinema.Reports
{
    public class PotentialRealProfitReportModel
    {
        public IEnumerable<PotentialRealProfitReportRow> Rows { get; set; }
    }
}