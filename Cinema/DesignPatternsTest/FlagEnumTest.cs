﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatternsTest
{
    public static class FlagEnumTest
    {
        [Flags]
        public enum TestFlag
        {
            //00000001 = 2^0
            First = 1,
            //00000010 = 2^1
            Second = 2,
            //00000100 = 2^2
            Third = 4,
            //00001000 = 2^3
            Fourth = 8,
            //00010000 = 2^4
            Fifths = 16,
            //00100000
            Sixth = 32,
            //01000000
            Seventh = 64,
            //10000000
            Eighth = 128
        }

        public static void ApplyCorrectValuesTest()
        {
            //00000001
            //00010000
            //+
            //00010001
            var testVar = TestFlag.First | TestFlag.Fifths;
            var intInterpretation = (int)testVar;
        }

        public static void ApplyCorrectValuesTest2()
        {
            //TestVar1
            //00000001
            //00010000
            //+
            //00010001

            //TestVar1
            //00000001
            //10000000
            //+
            //10000001

            //Sum
            //00010001
            //10000001
            //+
            //10010001
            var testVar = TestFlag.First | TestFlag.Fifths;
            var testVar2 = TestFlag.First | TestFlag.Eighth;
            var intInterpretation = (int)(testVar | testVar2);
        }

        public static void MultiplyValuesTest()
        {
            //00000001
            //00010000
            //*
            //00000000
            var testVar = TestFlag.First & TestFlag.Fifths;
            var intInterpretation = (int)testVar;
        }

        public static void ByteMultiplyTest()
        {
            //TestVar1
            //10010001

            //TestVar1
            //10000001

            //Sum
            //10010001
            //10000001
            //*
            //10000001
            var testVar = TestFlag.First | TestFlag.Fifths | TestFlag.Eighth;
            var testVar2 = TestFlag.First | TestFlag.Eighth;
            var intInterpretation = (int)(testVar & testVar2);
        }

        public static void ByteSumTest()
        {
            //TestVar1
            //10010001

            //TestVar2
            //00000110

            //Sum
            //10010001
            //00000110
            //+
            //10010111
            var testVar = TestFlag.First | TestFlag.Fifths | TestFlag.Eighth;
            var testVar2 = TestFlag.Second | TestFlag.Third;
            var sumResult = testVar | testVar2;
            var intResult = (int)sumResult;
        }

        public static void EqualityTest()
        {
            var testVar = TestFlag.First | TestFlag.Fifths;
            var isFlagSet = testVar == TestFlag.Fifths;
        }

        public static void HasFlagTest()
        {
            var testVar = TestFlag.First | TestFlag.Fifths | TestFlag.Eighth;
            var isFlagSet = testVar.HasFlag(TestFlag.Fifths | TestFlag.Eighth);
        }

        public static void FindDiffTest()
        {
            //10010111
            //10010011
            //?
            //00000100

            var testVar = TestFlag.First | TestFlag.Second | TestFlag.Third | TestFlag.Fifths | TestFlag.Eighth;
            var testVar2 = TestFlag.First | TestFlag.Second | TestFlag.Fifths | TestFlag.Eighth;
            var isFlagSet = testVar ^ testVar2;
        }
    }
}
