﻿namespace Cinema.Models.Tickets
{
    public enum ReportType
    {
        PotentialRealProfit,
        UnprofitableMovies
    }
}