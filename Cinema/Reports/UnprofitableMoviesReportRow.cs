﻿namespace Cinema.Reports
{
    public class UnprofitableMoviesReportRow
    {
        public string MovieName { get; set; }

        public float Profit { get; set; }
    }
}